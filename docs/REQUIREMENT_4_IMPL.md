[Back to Requirement 4](./REQUIREMENTS.md#4-insert-the-gif)

# Project Giphy - Requirements 4

## 4. Insert the GIF

Given that a user is using the comment editor <br>
When the user clicks the **Find and insert GIF** button, <br>
And the user clicks on a GIF <br>
Then the modal should close <br>
And Markdown for the selected image should be inserted in the last caret position of the textarea.

## 4.1. Click the new GIF

When the user selects a GIF from the list we will need to trigger a click event that should allow us to store some information based on the selected GIF.

```html
<script>
export default {
  methods: {
    selectGif(gif) {
      // Do something with the metadata from the GIF
    }
  }
}
</script>

<template>
   ...
   <gif-item
     v-for="gifs in gif" 
     :key="gif.id" 
     :gif="gif"
     @click="selectGif"
   />
</template>
```

## 4.2. Trigger modal close and emit event

With our triggered click event we need to now:

- Emit an event to the parent context for handling this gif selection.
- Close the modal.

```html
<script>
export default {
  methods: {
    selectGif(gif) {
      // Emit event for parent to handle
      this.$emit('select', gif);
      // Update showModal value to close modal
      this.showModal = false;
    }
  }
}
</script>
```

## 4.3. Update value to insert

Now that we have the metadata needed to insert into the Markdown, we need to handle the `select` event emitted from our Giphy modal from the parent context.

We can look at how the quote toolbar button already handles this over in [`src/vue_shared/components/markdown/header.vue`](https://gitlab.com/gitlab-ase/gitlab-giphy-project/blob/55ddab0836d9a7fb75169d409de29526d4f6403c/src/vue_shared/components/markdown/header.vue#L135).

Also, consider using the [`insertText` function from `common_utils`](https://gitlab.com/gitlab-ase/gitlab-giphy-project/blob/55ddab0836d9a7fb75169d409de29526d4f6403c/src/lib/utils/common_utils.js#L298).

```html
<script>
// from header.vue ...
import { getSelectedFragment, insertText } from '~/lib/utils/common_utils';
// ...

export default {
  methods: {
    // ...
    insertGif(gif) {
      const area = this.$el.parentNode.querySelector('textarea');
      const gfm = `![${value.title}](${value.url})`;

      insertText(area, gfm);
    }
  }
}
</script>
```

**Details:**

1. The newly-inserted Markdown should follow the
   [syntax for images](https://www.markdownguide.org/basic-syntax/#images-1). For example:
   `![alt text](https://example.com/url/to/image.png)`.
1. If the user had selected a block of text before selecting **Find and insert GIF**,
   the Markdown should **replace** the selected text.

![screenshot](giphy_4.png)

> **Hint:** <br>
> If the user selects a block of text in the textarea, opening the Giphy modal can
> clear out this selection data. Consider saving the
> [`selectionStart` and `selectionEnd` of the textarea](https://developer.mozilla.org/en-US/docs/Web/API/HTMLTextAreaElement)
> before opening the modal, so you can restore this state before inserting the new Markdown.

## References

- [VueJS Events](https://vuejs.org/guide/essentials/event-handling.html#listening-to-events)
- [Upstream issue](https://gitlab.com/gitlab-org/gitlab/-/issues/17379)
- [Giphy API documentation](https://developers.giphy.com/docs/api#quick-start-guide)
- [GitLab Flavored Markdown reference](https://docs.gitlab.com/ee/user/markdown.html)
